//var jsonData;
var PINS = {};
var currentPin = null;
function load() {
   $('#proyectModal').modal('show');
	var graph = document.getElementById("graph");
	loadJSONView();
}

function add() {
	var graph = document.getElementById("graph");
   var pin = document.querySelector('[name="paData[CPIN]"]');
   if (pin.value == "") {
      console.log("Pin no valido");
      return;
   }
	if (PINS[pin.value] !== undefined) {
		console.log("Already added");
		return;
	}
	PINS[pin.value] = {
		state: 0,
		values: []
	};

	var item = document.createElement("div");
	var title = document.createElement("div");
   var checkbox = document.createElement("input");
   var spanTitle = document.createElement("span");
	checkbox.setAttribute("type", "checkbox");
   checkbox.setAttribute("onclick", "changeFH('" + pin.value + "', this)");
   spanTitle.innerHTML = pin.value;
	title.appendChild(checkbox);
   title.appendChild(document.createTextNode("  "));
	title.appendChild(spanTitle);
	item.appendChild(title);
	item.setAttribute("id", pin.value);
	item.style.overflowX = "auto";
	item.style.padding = "3px";
	item.style.marginBottom = "5px";
	item.className = "shadow p-2 pinWrapper";
	item.style.position = "relative";
   /*item.style.boxShadow = "1px 1px 5px black";*/
   item.val = pin.value;
   item.onclick = function(e) {
      $('.pinWrapper').removeClass('border border-warning');
      $(item).addClass('border border-warning');
      $('#btnAddTime').removeAttr('disabled');
      $('#interval').removeAttr('disabled');
      currentPin = item.val;
   };
   title.ondblclick = function() {
      var lcTitle = prompt('Nombre del pin ' + item.val);
      if (lcTitle != null && lcTitle != "") {
         spanTitle.innerText = lcTitle;
      }
   };
	graph.appendChild(item);
	loadJSONView();
}
function changeFH(id, checkbox)
{
	PINS[id].state = checkbox.checked ? 1 : 0;
	color(id);
	loadJSONView();
}
function loadViewByJSON(json) {
	
}
function saveProject() {
	var win = window.open();
}
function loadJSONView() {
	var jsonView = document.getElementById("jsonView");
	var repeat = $('[name="paData[NREPET]"]').val();
	var pins = [];
	for(var i in PINS) {
		var pin = {
			name: i,
			state: PINS[i].state,
			values: PINS[i].values
		};
		pins.push(pin);
	}
	//"repeat": (repeat > 0 ? repeat : (repeat == 0 ? false : true)),
	var json = {
		"name": $('[name="paData[CNOMPRO]"]').val(),
		"pic": $('[name="paData[CPIC]"]').val(),
		"repeat": (repeat > 0 ? repeat : (repeat == 0 ? true : true)),
		"pins": pins
	};
	jsonView.innerHTML = JSON.stringify(json);
}

function addTime() {
   var selected = currentPin;
	var value = document.getElementById("interval").value;
	if(selected == '' || value == '' || document.getElementById(selected) === null){
		console.log("Sin elementos");
		return;
	}
	var item = document.createElement("div");
   /*item.style.position = "absolute";*/
   item.className = "pointer";
	item.style.display = "inline-block";
	item.style.paddingTop = "4px";
	item.style.paddingBottom = "4px";
	item.style.marginTop = "4px";
	item.style.marginBottom = "4px";
	item.style.textAlign = "center";
	item.style.width = (0.05 * value) + "px";
	item.appendChild(document.createTextNode(value));
	document.getElementById(selected).appendChild(item);
	PINS[selected].values.push(parseInt(value));
	color(selected);
   loadJSONView();
   item.onclick = function() {
      if (confirm('Eliminar')) {
         var lnIdx = item.parentElement.childElementCount - 2;
         PINS[item.parentElement.id].values.splice(lnIdx, 1);
         $(item).remove();
         color(selected);
         loadJSONView();
      }
   }
}

function color(selected) {
	var state = PINS[selected].state;
	var divs = document.getElementById(selected);
	for (var i = 1; i < divs.children.length; i++) {
		divs.children[i].style.border = "none";
		if (state) {
			if (i % 2 == 0) {
				divs.children[i].style.borderBottom = "2px solid green";
			}
			else {
				divs.children[i].style.borderTop = "2px solid red";
			}
		}
		else {
			if (i % 2 == 1) {
				divs.children[i].style.borderBottom = "2px solid green";
			}
			else {
				divs.children[i].style.borderTop = "2px solid red";
			}
		}
	}
}
function generateCode() {
	var jsonData = document.getElementById("jsonView").value;
	$.ajax({
		url: 'generateCode.php',
		type: 'POST',
		data: {
			json: jsonData
		},
		success: function(data){
			if(data.success)
				document.getElementById("code").innerHTML = data.code;
		},
		error: function(request, status, error){
			console.log(request);
		}
	});
}
function loadPins(p_cPic) {
	var pin = document.querySelector('[name="paData[CPIN]"]');
   $.post('getPins.php', {
      pic: p_cPic
   }, function(json) {
      if (json.success) {
         pin.innerHTML = '<option disabled selected value="">Seleccione pin</option>';
         for(var i in json.data) {
            var option1 = document.createElement("option");
            option1.innerHTML = json.data[i];
            pin.appendChild(option1);
			}
			$(pin).removeAttr('disabled');
      }
   });
	loadJSONView();
}
function f_toggle_repetir(p_oBtn) {
   if ($('#nrepetir').hasClass('d-none')) {
      $('#nrepetir').removeClass('d-none');
      $(p_oBtn).find('i').removeClass('text-success');
   } else {
      $('#nrepetir').addClass('d-none');
      $('[name="paData[NREPET]"]').val(0);
      $(p_oBtn).find('i').addClass('text-success');
	}
	loadJSONView();
}
let App = {
   lnIdProy: null,
   jsonData: null,
   graph: null,
   Init: function() {
      this.graph = document.querySelector("#graph");
   },
   OpenProject: function(p_oBtn) {
      $.post('Main/OpenProject', {
         paData: {
            NIDPROY: $(p_oBtn).attr('nidproy')
         }
      }, function(p_oResp) {
         if (p_oResp.ERROR) {
            console.log(p_oResp.ERROR);
         } else {
            App.jsonData = JSON.parse(p_oResp.MDATA);
            App.JsonToUI();
            App.lnIdProy = p_oResp.NIDPROY;
            $('#proyectModal').modal('hide');
         }
      });
   },
   JsonToUI: function() {
      this.jsonData.pins.forEach(laPin => {
         this.AddPin(laPin.name);
         laPin.values.forEach(lcValue => {
            this.AddTime(lcValue, laPin.name);
         });
      });
   },
   AddPinUI: function() {
      var lcPin = document.querySelector('[name="paData[CPIN]"]').value;
      this.AddPin(lcPin);
   },
   AddPin: function(p_cPin) {
      if (p_cPin == "") {
         console.log("Pin no valido");
         return;
      }
      if (PINS[p_cPin] !== undefined) {
         console.log("Already added");
         return;
      }
      PINS[p_cPin] = {
         state: 0,
         values: []
      };

      var loItem = $('<div>').addClass('shadow p-2 pinWrapper row p-2 mb-3').css({
         overflowX: 'auto',
         position: 'relative'
      }).attr({val: p_cPin, id: p_cPin});
      var loTitle = $('<div>');
      var loCheck = $('<input>').attr({type:'checkbox', 'onclick': "changeFH('" + p_cPin + "', this)"});
      var loSpanTitle = $('<div>').html(p_cPin);
      loTitle.append(loCheck);
      loTitle.append(document.createTextNode(" "));
      loTitle.append(loSpanTitle);
      loItem.append(loTitle);
      loItem.click(function() {
         $('.pinWrapper').removeClass('border border-warning');
         loItem.addClass('border border-warning');
         $('#btnAddTime').removeAttr('disabled');
         $('#interval').removeAttr('disabled');
         currentPin = loItem.attr('val');
      });
      loItem.dblclick(function() {
         var lcTitle = prompt('Nombre del pin ' + loItem.attr('val'));
         if (lcTitle != null && lcTitle != "") {
            loSpanTitle.text(lcTitle);
         }
      });
      $(this.graph).append(loItem);
      //loadJSONView();
   },
   AddTimeUI: function() {
      var lnTime = document.getElementById("interval").value;
      var lcPin = currentPin;
      this.AddTime(lnTime, lcPin);
   },
   AddTime: function(p_nTime, p_cPin) {   
      if (p_cPin == '' || p_nTime == '' || document.getElementById(p_cPin) === null){
         console.log("Sin elementos");
         return;
      }
      var loItem = $('<div>').addClass('pointer').text(p_nTime);
      loItem.css({
         display: 'inline-block',
         textAlign: 'center',
         width: (0.05 * p_nTime) + "px"
      });
      // var item = document.createElement("div");
      ///*item.style.position = "absolute";*/
      // item.className = "pointer";
      // item.style.display = "inline-block";
      // item.style.paddingTop = "4px";
      // item.style.paddingBottom = "4px";
      // item.style.marginTop = "4px";
      // item.style.marginBottom = "4px";
      // item.style.textAlign = "center";
      // item.style.width = (0.05 * p_nTime) + "px";
      // item.appendChild(document.createTextNode(p_nTime));
      $(`#${p_cPin}`).append(loItem);
      // document.getElementById(p_cPin).appendChild(item);
      PINS[p_cPin].values.push(parseInt(p_nTime));
      color(p_cPin);
      loadJSONView();
      loItem.click(function() {
         if (confirm('Eliminar')) {
            var lnIdx = loItem.parent().children.length - 2;
            PINS[loItem.parent().attr('id')].values.splice(lnIdx, 1);
            loItem.remove();
            color(p_cPin);
            loadJSONView();
         }
      });
      // item.onclick = function() {
      //    if (confirm('Eliminar')) {
      //       var lnIdx = item.parentElement.childElementCount - 2;
      //       PINS[item.parentElement.id].values.splice(lnIdx, 1);
      //       $(item).remove();
      //       color(p_cPin);
      //       loadJSONView();
      //    }
      // }
   }
};
$(document).ready(function() {
   App.Init();
   $('#proyectModal').on('hide.bs.modal', function() {
      if (App.lnIdProy == null && !($('#createModal').data('bs.modal') || {})._isShown) {
         var elem = $(this);
         setTimeout(function() {
            elem.modal('show');
         }, 100);
      }
   });

   $('#createModal').on('hide.bs.modal', function() {
      $('#proyectModal').modal('show');
   });
});
// UI
function f_showCreateModal() {
   $('#createModal').modal('show');
   $('#proyectModal').modal('hide');
}
function f_createProject() {
   console.log('f_createProyect');
   $.post('Main/CreateProject', {
      
      paData: {
         CNOMBRE: $('[name="paData[CNOMBRE]"]').val(),
         CCODPIC: $('[name="paData[CPIC]"]').val(),
         NREPET: $('[name="paData[NREPET]"]').val()
      }
   }, function (p_oResp) {
      console.log(p_oResp);
   });
}
function f_openProject(p_oBtn) {
   $.post('Main/OpenProject', {
      paData: {
         NIDPROY: $(p_oBtn).attr('nidproy')
      }
   }, function(p_oResp) {
      if (p_oResp.ERROR) {
         console.log(p_oResp.ERROR);
      } else {
         jsonData = JSON.parse(p_oResp.MDATA);
         f_jsonToUI();
         App.lnIdProy = p_oResp.NIDPROY;
         $('#proyectModal').modal('hide');
      }
   });
}