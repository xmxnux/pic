<?php
header("Content-Type: application/json");
if(isset($_REQUEST['json'])){
	$json = $_REQUEST['json'];
	$handler = fopen("code.json", "w");
	fwrite($handler, $json);
	fclose($handler);
	$code = exec("./CCSCodeGenerator/CCSCodeGenerator code.json > code.txt");
	echo json_encode(array(
		"success" => true,
		"code" => file_get_contents("code.txt")
	));
}
else{
	echo json_encode(array(
		"success" => false,
		"msg" => "Incomplete params"
	));
}
?>