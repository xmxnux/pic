<?php
require_once('Clases/CBase.php');
require_once('Clases/CSql.php');
class CCodeGenerator extends CBase {
   public function omCreateProject() {
      $llOk = $this->mxValParamCreateProject();
      if (!$llOk) {
         return false;
      }
      $loSql = new CSql();
      $llOk = $loSql->omConnect();
      if (!$llOk) {
         $this->pcError = $loSql->pcError;
         return false;
      }
      $llOk = $this->mxCreateProject($loSql);
      if (!$llOk) {
         $loSql->rollback();
      }
      $loSql->omDisconnect();
      return $llOk;
   }

   private function mxValParamCreateProject() {
      if (!isset($this->paData['CNOMBRE']) || strlen($this->paData['CNOMBRE']) == 0) {
         $this->pcError = "NOMBRE DEL PROYECTO NO DEFINIDO O NO VALIDO";
         return false;
      } elseif (!isset($this->paData['NREPET']) || !preg_match('(true|\d+)', $this->paData['NREPET'])) {
         $this->pcError = "REPETICIONES NO DEFINIDO O NO VALIDO";
         return false;
      } elseif (!isset($this->paData['CCODPIC']) || strlen($this->paData['CCODPIC']) == 0) {
         $this->pcError = "PIC NO DEFINIDO O NO VALIDO";
         return false;
      }
      return true;
   }

   private function mxCreateProject($p_oSql) {
      $lcJson = json_encode($this->paData);
      $lcSql = "SELECT P_A01MPRY_1($1)";
      $R1 = $p_oSql->omExec($lcSql, [$lcJson]);
      if (!$laTmp = $p_oSql->fetch($R1)) {
         $this->pcError = "ERROR EN CONSULTA";
         return false;
      }
      $laJson = json_decode($laTmp[0], true);
      if (isset($laJson['ERROR'])) {
         $this->pcError = $laJson['ERROR'];
         return false;
      }
      $this->paData = $laJson;
      return true;
   }

   public function omGetRecents() {
      $loSql = new CSql();
      $llOk = $loSql->omConnect();
      if (!$llOk) {
         $this->pcError = $loSql->pcError;
         return false;
      }
      $llOk = $this->mxGetRecents($loSql);
      $loSql->omDisconnect();
      return $llOk;
   }

   private function mxGetRecents($p_oSql) {
      $lcSql = "SELECT nIdProy, cNombre, cCodPic, nRepet FROM A01MPRY WHERE cEstado = 'A'";
      $R1 = $p_oSql->omExec($lcSql);
      $this->paDatos = [];
      while ($laFila = $p_oSql->fetch($R1)) {
         $this->paDatos[] = ['NIDPROY' => $laFila[0], 'CNOMBRE' => $laFila[1],
                             'CCODPIC' => $laFila[2], 'NREPET'  => $laFila[3]];
      }
      return true;
   }

   public function omGetPics() {
      $loSql = new CSql();
      $llOk = $loSql->omConnect();
      if (!$llOk) {
         $this->pcError = $loSql->pcError;
         return false;
      }
      $llOk = $this->mxGetPics($loSql);
      $loSql->omDisconnect();
      return $llOk;
   }

   private function mxGetPics($p_oSql) {
      $lcSql = "SELECT cCodPic FROM A01MPIC WHERE cEstado = 'A'";
      $R1 = $p_oSql->omExec($lcSql);
      $this->paDatos = [];
      while ($laFila = $p_oSql->fetch($R1)) {
         $this->paDatos[] = ['CCODPIC' => $laFila[0]];
      }
      if (count($this->paDatos) == 0) {
         $this->pcError = "SIN PICS ENCONTRADOS";
         return false;
      }
      return true;
   }

   public function omOpenProject() {
      $llOk = $this->mxValParamOpenProject();
      if (!$llOk) {
         return false;
      }
      $loSql = new CSql();
      $llOk = $loSql->omConnect();
      if (!$llOk) {
         $this->pcError = $loSql->pcError;
         return false;
      }
      $llOk = $this->mxOpenProject($loSql);
      $loSql->omDisconnect();
      return $llOk;
   }

   private function mxValParamOpenProject() {
      if (!isset($this->paData['NIDPROY']) || !preg_match('(^\d+$)', $this->paData['NIDPROY'])) {
         $this->pcError = "ID DEL PROYECTO NO DEFINIDO O NO VALIDO";
         return false;
      }
      return true;
   }

   private function mxOpenProject($p_oSql) {
      $lcSql = "SELECT nIdProy, cNombre, cCodPic, mData 
                FROM A01MPRY
                WHERE nIdProy = $1 AND cEstado = 'A'";
      $R1 = $p_oSql->omExec($lcSql, [$this->paData['NIDPROY']]);
      $laTmp = $p_oSql->fetch($R1);
      if (!$laTmp) {
         $this->pcError = "PROYECTO NO ENCONTRADO";
         return false;
      }
      $this->paData = ['NIDPROY' => $laTmp[0], 'CNOMBRE' => $laTmp[1],
                       'CCODPIC' => $laTmp[2], 'MDATA'   => $laTmp[3]];
      return true;
   }
}
?>