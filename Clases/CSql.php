<?php
class CSql {
   public $pcError;

   public function __construct() {
      $this->pcError = null;
   }

   public function omConnect() {
      $lcConStr = "host=localhost dbname=pic port=5432 user=postgres password=1921681m3";
      @$this->h = pg_connect($lcConStr)  or die("Can't connect to database".pg_last_error());
      if (!$this->h) {
         $this->pcError = "No se pudo conectar a la base de datos";
         return false;
      }
      $this->omExec("BEGIN;");
      return true;
   }

   public function omExec($p_cSql, $p_aParams = []) {
      if (strpos($p_cSql, 'UPDATE') !== false || strpos($p_cSql, 'INSERT') !== false || strpos($p_cSql, 'NOTIFY') !== false) {
         @$RS = pg_query_params($this->h, $p_cSql, $p_aParams);
         if (pg_affected_rows($RS) == 0) {
            if (!($RS)) {
               $this->pcError = "La operacion no afecto a ninguna fila";
               return false;
            }
         }
         return true;
      } else {
         $this->pnNumRow = 0;
         @$RS = pg_query_params($this->h, $p_cSql, $p_aParams);
         if (!($RS)) {
            $this->pcError = "Error al ejecutar comando SQL";
            return false;
         }
         $this->pnNumRow = pg_num_rows($RS);
         return $RS;
      }
   }

   public function omDisconnect() {
      $this->omExec("COMMIT;");
      pg_close($this->h);
      $this->h = NULL;
   }

   public function fetch($RS) {
      if ($RS === false) {
         return false;
      }
      return pg_fetch_row($RS);
   }

   public function rollback() {
      $this->omExec("ROLLBACK;");
   }

   public function omNotify($p_cValue) {
      $llConnect = $this->h == NULL;
      if ($llConnect) {
         $llOk = $this->omConnect();
         if (!$llOk) {
            return false;
         }
      }
      $llOk = $this->omExec("NOTIFY DATA, '$p_cValue';");
      if ($llConnect) {
         $this->omDisconnect();
      }
      return $llOk;
   }
}
?>