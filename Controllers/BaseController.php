<?php
require_once("Libs/Smarty/Smarty.class.php");
class BaseController {
   protected $loSmarty;
   public function __construct() {
      $this->loSmarty = new Smarty();
   }
   protected function display($p_cView, $p_cError = null) {
      $this->loSmarty->display($p_cView);
   }
   protected function assign($p_cName, $p_cValue) {
      $this->loSmarty->assign($p_cName, $p_cValue);
   }
}
function setJSON() {
   header('Content-Type: application/json');
}
?>