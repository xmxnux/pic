<?php
require_once('Controllers/BaseController.php');
require_once('Clases/CCodeGenerator.php');
class MainController extends BaseController {
   public function Index() {
      $lo = new CCodeGenerator();
      $llOk = $lo->omGetRecents();
      if (!$llOk) {
         echo $lo->pcError;
         return;
      }
      $laArrays['ARECENT'] = $lo->paDatos;
      $llOk = $lo->omGetPics();
      if (!$llOk) {
         echo $lo->pcError;
         return;
      }
      $laArrays['APIC'] = $lo->paDatos;
      $this->assign('saArrays', $laArrays);
      $this->display('Views/Main.tpl');
   }

   public function CreateProject() {
      $lo = new CCodeGenerator();
      $lo->paData = $_REQUEST['paData'];
      $llOk = $lo->omCreateProject();
      setJSON();
      if (!$llOk) {
         echo json_encode(['ERROR' => $lo->pcError]);
      } else {
         echo json_encode($lo->paData);
      }
   }

   public function OpenProject() {
      $lo = new CCodeGenerator();
      $lo->paData = $_REQUEST['paData'];
      $llOk = $lo->omOpenProject();
      setJSON();
      if (!$llOk) {
         echo json_encode(['ERROR' => $lo->pcError]);
      } else {
         echo json_encode($lo->paData);
      }
   }
}
?>