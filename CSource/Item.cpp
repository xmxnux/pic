#include <string>
using namespace std;
class Item
{
public:
	Item()
	{

	}
	Item(string name, int time, bool state)
	{
		this->initTime = time;
		this->name = name;
		this->state = state;
	}
	string getName()
	{
		return this->name;
	}
	bool operator< (const Item &b) const
	{
		return this->initTime > b.initTime;
	}
	bool getState()
	{
		return this->state;
	}
	void setState(bool state)
	{
		this->state = state;
	}
	int getTime()
	{
		return this->initTime;
	}
	string toString()
	{
		return name + ", " + to_string(initTime) + ", " + (state ? "high" : "low") + "\n";
	}
	string print()
	{
		return (state ? "output_high(" : "output_low(") + name + ");";
	}
private:
	string name;
	int initTime;
	bool state;
};