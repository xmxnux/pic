#include <iostream>
#include <queue>
#include <fstream>
#include "json.hpp"
#include "Item.cpp"
using namespace std;
using json = nlohmann::json;
int main(int argc, char **argv)
{
	string s;
	json j;
	bool repeat;
	if (argc == 2)
	{
		ifstream i(argv[1]);
		i >> j;
	}
	else {
		getline(cin, s);
		j = json::parse(s);
	}
	cout << "//pic: " << j["pic"] << endl;
	cout << "//name: " << j["name"] << endl;
	if (j["repeat"].is_boolean() && j["repeat"].get<bool>())
	{
		repeat = true;
		cout << "while(1){\n";
	}
	else if(j["repeat"].is_number()) {
		repeat = true;
		cout << "int veces = " << j["repeat"].get<int>() << ";\n";
		cout << "while(veces--){\n";
	}
	priority_queue<Item> cola;
	auto pins = j["pins"];
	for (json::iterator it = pins.begin(); it != pins.end(); it++) 
	{
		auto current = *it;
		bool high;
		if (current["state"] == 1)
		{
			if (repeat) cout << "\t";
			cout << "output_high(" << current["name"].get<string>() << ");" << endl;
			high = false;
		}
		else
		{
			if (repeat) cout << "\t";
			cout << "output_low(" << current["name"].get<string>() << ");" << endl;
			high = true;
		}
		auto values = current["values"];
		int initValue = 0;
		for (json::iterator cvit = values.begin(); cvit != values.end(); cvit++)
		{
			auto cv = *cvit;
			initValue += cv.get<int>();
			cola.push(Item(current["name"], initValue, high));
			high = !high;
		}
		//cout << current << endl;
	}
	int time = 0;
	while (!cola.empty())
	{
		Item c = cola.top();
		if (time < c.getTime())
		{
			if (repeat) cout << "\t";
			cout << "delay_ms(" << c.getTime() - time << ");" << "\n\n";
			time = c.getTime();
		}
		if (repeat) cout << "\t";
		cout << c.print() << endl;
		if(cola.size() == 1)
                {
                        if(repeat) cout << "\t";
                        cout << "delay_ms(" << c.getTime() - time << ");" << "\n\n";
                }
		cola.pop();
	}
	if (repeat)
	{
		cout << "}\n";
	}
	//system("pause");
}
