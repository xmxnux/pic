import json
import heapq
lcJson = '{"name":"","pic":"16f628a","repeat":true,"pins":[{"name":"PIN_A0","state":0,"values":[1000,2000,1000]},{"name":"PIN_A1","state":1,"values":[3000,1000]},{"name":"PIN_A2","state":0,"values":[2000,2000]}]}'
class CItem:
   def __init__(self, p_cName, p_nInitTime, p_lState):
      self.lcName = p_cName
      self.lnInitTime = p_nInitTime
      self.llState = p_lState
   def toString(self):
      return '%s, %d, %s'%(self.lcName, self.lnInitTime, 'high' if self.llState else 'low')
   def printS(self):
      return 'output_%s(%s);'%('high' if self.llState else 'low', self.lcName)
   def __lt__(self, p_oItem):
      return self.lnInitTime < p_oItem.lnInitTime
class CCodeGenerator:
   def omProccess(self):
      llOk = self.mxReadJSON()
      if not llOk:
         return False
      llOk = self.mxProccess()
      return llOk

   def mxReadJSON(self):
      self.paData = json.loads(lcJson)
      if not 'pins' in self.paData or len(self.paData['pins']) == 0:
         self.pcError = "Pines no definidos"
         return False
      return True

   def mxProccess(self):
      # Infomacion del proyecto
      print('// pic: %s'%(self.paData['pic']))
      print('// name: %s'%(self.paData['name']))
      # Loop
      if isinstance(self.paData['repeat'], bool) and not isinstance(self.paData['repeat'], int):
         print("while(1) {")
      if isinstance(self.paData['repeat'], int):
         print("int veces = %d"%(self.paData['repeat']))
         print("while(veces--) {")
      # Pines
      laPines = self.paData['pins']
      laDatos = []
      for laPin in laPines:
         llHigh = (laPin['state'] == 1)
         print('\toutput_%s(%s);'%('high' if llHigh else 'low', laPin['name']))
         llHigh = not llHigh
         laValues = laPin['values']
         lnInitVal = 0
         for lnValue in laValues:
            lnInitVal += lnValue
            heapq.heappush(laDatos, CItem(laPin['name'], lnInitVal, llHigh))
            llHigh = not llHigh
      lnTime = 0
      while len(laDatos) > 0:
         loItem = heapq.heappop(laDatos)
         if lnTime < loItem.lnInitTime:
            print('\tdelay_ms(%d);'%(loItem.lnInitTime - lnTime))
            lnTime = loItem.lnInitTime
         print('\t' + loItem.printS())
         if len(laDatos) == 1 and (loItem.lnInitTime - lnTime) != 0:
            print('\tdelay_ms(%d);'%(loItem.lnInitTime - lnTime))
      print('}')
      return True
      
if __name__ == "__main__":
   lo = CCodeGenerator()
   llOk = lo.omProccess()
   if not llOk:
      print(lo.pcError)