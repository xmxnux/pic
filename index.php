<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
session_start();
$controller = "Main";
if (!isset($_REQUEST['c'])) {
   require_once("Controllers/{$controller}Controller.php");
   $controller = $controller."Controller";
   $controller = new $controller;
   $controller->Index();
} else {
   $controller = $_REQUEST['c'];
   if (file_exists("Controllers/{$controller}Controller.php")) {
      require_once("Controllers/{$controller}Controller.php");
      if (isset($_REQUEST['a'])) {
         $accion = $_REQUEST['a'];
         $controller = $controller."Controller";
         $controller = new $controller;
         if (!method_exists($controller, $accion)) {
            return;
         }
         call_user_func([$controller, $accion]);
      } elseif (class_exists($controller)) {
         $controller = new $controller;
         $controller->Index();
      } else {
         echo "ERROR";
      }
   } else {
      echo "ERROR";
   }
}
?>