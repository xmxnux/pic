<?php
header("Content-Type: application/json");
if(isset($_REQUEST['pic'])){
	$pic = $_REQUEST['pic'];
	$pins = array();
	switch ($pic) {
      case '16f629':
         $pins = array(
            'PIN_A0',
            'PIN_A1',
            'PIN_A2',
            'PIN_A3',
            'PIN_A4',
            'PIN_A5'
         );
         break;
		case '16f628a':
			$pins = array(
            'PIN_A0',
				'PIN_A1',
				'PIN_A2',
				'PIN_A3',
				'PIN_A4',
				'PIN_A5',
				'PIN_A6',
				'PIN_A7',
				'PIN_B0',
				'PIN_B1',
				'PIN_B2',
				'PIN_B3',
				'PIN_B4',
				'PIN_B5',
				'PIN_B6',
				'PIN_B7'
			);
			break;		
		case '16f778a':
			$pins = array(
            'PIN_A0',
				'PIN_A1',
				'PIN_A2',
				'PIN_A3',
				'PIN_A4',
				'PIN_A5',
				'PIN_B0',
				'PIN_B1',
				'PIN_B2',
				'PIN_B3',
				'PIN_B4',
				'PIN_B5',
				'PIN_B6',
            'PIN_B7',
            'PIN_C0',
				'PIN_C1',
				'PIN_C2',
				'PIN_C3',
				'PIN_C4',
				'PIN_C5',
				'PIN_C6',
            'PIN_C7',
            'PIN_D0',
				'PIN_D1',
				'PIN_D2',
				'PIN_D3',
				'PIN_D4',
				'PIN_D5',
				'PIN_D6',
				'PIN_D7'
			);
			break;
	}
	echo json_encode(array(
		"success" => true,
		"data" => $pins
	));
}
else{
	echo json_encode(array(
		"success" => false,
		"msg" => "Incomplete params"
	));
}
?>